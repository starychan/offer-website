<?php
/**
 * User: oscar
 * Date: 3/15/15
 * Time: 11:21 上午
 */

date_default_timezone_set('PRC');

class dbManager {

//    local database setting
      // private $servername = "localhost";
      // private $username = "root";
      // private $password = "1234";
      // private $dbname = "offerdb";

//    remote database setting
      private $servername = "127.0.0.1";
      private $username = "root";
      private $password = "leidotech";
      private $dbname = "offerdb";

   //Test database setting
//       private $servername = "223.223.218.172";
//       private $username = "root";
//       private $password = "leidotech";
//       private $dbname = "test";

    public function getConn(){
        $conn = new mysqli($this->servername,$this->username,$this->password,$this->dbname);
        $conn->set_charset("utf8");

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        return $conn;
    }

    public function saveToResInfo($params){

        global $msg;
        global $userId;

        //set params
        $userId = $params['userId'];
        $username = $params['username'];
        $password = $params['password'];
        $psHint = $params['psHint'];
        $htAnswer = $params['htAnswer'];
        $gen_date = date("Ymd");
        $lastLoginDate = date("Ymd");
        $login_count = 1;
        $email = $username;
        $user_auth = "0";//0:未通过验证 1:已通过验证
        $userStatus = "1"; // 0:用户已失效 1:用户生效

        //init customized params
        $savRes = true;
        $conn = new mysqli($this->servername,$this->username,$this->password,$this->dbname);
        $conn->set_charset("utf8");
        $msg = "Noting has been done";

        //init sql
        $sql = "INSERT INTO register_info (user_id,username,password,ps_hint,ht_answer,gen_date,last_login_date,login_count,email,user_auth,user_status)
                VALUES ('$userId','$username','$password','$psHint','$htAnswer','$gen_date','$lastLoginDate',$login_count,'$email','$user_auth','$userStatus')";

        if ($conn->query($sql) === TRUE) {
            $msg = "New record created into register successfully";
        } else {
            $msg =  "Error: " . $sql . "<br>" . $conn->error;
            $savRes = false;
        }

        $conn->close();

        //init the result set for passing back
        $res = array(
            'savRes' => $savRes,
            'userId' => $userId,
            'msg'    => $msg
        );
        return $res;
    }

    public function dicParse($dic_name,$dic_val){

        $conn = $this->getConn();
        //define sql
        $sql = "SELECT cn_name FROM dict_info WHERE dict_name_en = '$dic_name' and dict_value = '$dic_val'";

        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            $dic_cn = $result->fetch_assoc();
        }else{
             "No data found. <br>Error: " . $sql . "<br>" . $conn->error;
        }

        $result->close();
        $conn->close();

        return $dic_cn['cn_name'];
    }

    public function dicParseEn($dic_name,$dic_val){

        $conn = $this->getConn();
        //define sql
        $sql = "SELECT en_name FROM dict_info WHERE dict_name_en = '$dic_name' and dict_value = '$dic_val'";

        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            $dic_cn = $result->fetch_assoc();
        }else{
            "No data found. <br>Error: " . $sql . "<br>" . $conn->error;
        }
        $result->close();
        $conn->close();
        return $dic_cn['en_name'];
    }

    public function checkUserName($username){

        $conn = new mysqli($this->servername,$this->username,$this->password,$this->dbname);
        //check connection
        if ($conn->connect_errno) {
            $res['errorMsg'] =  "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
        }
        //define sql
        $sql = "SELECT count(1) num FROM register_info WHERE username = '$username'";

        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            $res = $result->fetch_assoc();
        }else{
            $res['errorMsg'] = "No data found. <br>Error: " . $sql . "<br>" . $conn->error;
        }


        $result->close();
        $conn->close();
        return $res['num'];
    }

    public function countuser(){

        $backdata = array();

        $backdata['num'] = $this->count("SELECT count(1) num FROM register_info");
        $backdata['tm_num'] = $this->count("SELECT count(1) tm_num FROM tm_reg_info");

        return $backdata;

    }

    public function count($sql){
        $conn = $this->getConn();
        //define sql
        $result = $conn->query($sql);
        if ($result->num_rows > 0){
            $res = $result->fetch_assoc();
        }else{
            $res['errorMsg'] = "No data found. <br>Error: " . $sql . "<br>" . $conn->error;
        }
        $result->close();
        $conn->close();
        return $res;
    }

    public function checkLogin($username,$password){

        $conn = $this->getConn();
        $res = array();

        //check connection
        if ($conn->connect_errno) {
            $res['msg'] = "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
        }
        //define sql
        $sql = " select user_id,password,login_count from register_info where username = '$username';";
        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            $userInfo = $result->fetch_assoc();
            $msg = $userInfo['password'] === $password ? "success":"failed";
            $res['msg'] = $msg;
            $res['userId'] = $userInfo['user_id'];

            if($msg === "success"){
                $login_count = $userInfo['login_count'];
                $date = date("Ymd");
                $sql = "update register_info set login_count = $login_count+1 where username = '$username'";
                $conn->query($sql);
                $sql = "update register_info set last_login_date = '$date' where username = '$username'";
                $conn->query($sql);
            }
        }else{
            $res['msg'] = "no_record";
            $res['errorMsg'] = "No data found. <br>Error: " . $sql . "<br>" . $conn->error;
        }

        //set passing back data
        $res['$sql'] = $sql;

        $conn->close();
        return $res;
    }

    public function selUserInfo($userId){

        global $res;

        $conn = $this->getConn();

        $sql = "SELECT * FROM register_info re INNER JOIN user_info us WHERE re.user_id = '$userId' and re.user_id=us.user_id";
        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            $res = $result->fetch_assoc();
            $res['msg'] = "success";
        }else{
            $res['msg'] = "failed";
            $res['errorMsg'] = "No data found. <br>Error: " . $sql . "<br>" . $conn->error;
        }

        $res['comPercent'] = $this->getComPercent($userId);
        $res['is_leader'] = $this->isLeader($userId);
        $res['has_team'] = $this->ifHasTeam($userId);
        $result->close();
        $conn->close();
        return $res;
    }

    public function ifHasTeam($pUserId){
        $conn = $this->getConn();

        $sql = "SELECT * FROM tm_member where tm_member_id = '$pUserId'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            $res = true;
        }else{
            $res = false;
        }
        return $res;
    }

    public function isLeader($userId){
        $conn = $this->getConn();
        $sql = "SELECT * FROM tm_member where tm_leader_id = '$userId'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0){
            return true;
        }else{
            return false;
        }
    }

    public function selUserInfoByUserName($pUserName){

        $conn = $this->getConn();
        $sql = "SELECT * FROM register_info re INNER JOIN user_info us WHERE re.username = '$pUserName' and re.user_id=us.user_id";
        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            $res = $result->fetch_assoc();
            $res['ps_hint'] = $this->dicParse("dic_pshint",$res['ps_hint']);
            $res['msg'] = "success";
        }else{
            $res['msg'] = "failed";
            $res['errorMsg'] = "No data found. <br>Error: " . $sql . "<br>" . $conn->error;
        }

        $result->close();
        $conn->close();

        return $res;
    }

    public function checkPsAnswer($pUserName){

        $conn = $this->getConn();
        $sql = "SELECT ht_answer FROM register_info  WHERE username = '$pUserName'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            $res = $result->fetch_assoc();
            $res['msg'] = "success";
        }else{
            $res['msg'] = "failed";
            $res['errorMsg'] = "No data found. <br>Error: " . $sql . "<br>" . $conn->error;
        }

        $result->close();
        $conn->close();

        return $res;
    }

    public function getComPercent($userId){
        $conn = new mysqli($this->servername,$this->username,$this->password,$this->dbname);
        $conn->set_charset("utf8");

        $sql = "SELECT
                  ((CASE WHEN rel_status ='' THEN 0 ELSE 5 END)
                   + (CASE WHEN CHAR_LENGTH(personality)>10 THEN 10 ELSE CHAR_LENGTH(personality) END)
                  + (CASE WHEN CHAR_LENGTH(habit) > 10 THEN 5 ELSE floor((CHAR_LENGTH(habit)/10)*5) END)
                  + (CASE WHEN CHAR_LENGTH(remark) > 20 THEN 10 ELSE floor((CHAR_LENGTH(remark)/20)*10) END)
                  + (CASE WHEN head_icon ='' THEN 0 ELSE 10 END)
                  + (CASE WHEN CHAR_LENGTH(others) >20 THEN 10 ELSE floor((CHAR_LENGTH(others)/20)*10) END)+50)
                  AS sum
                from user_info
                where user_id = '$userId'";
        $result = $conn->query($sql);
        $comPercent = $result->fetch_assoc();

        $conn->close();

        return $comPercent['sum'];
    }

    public function updateUserInfo($params){

        global $res;
        $res = array();
        //user unique id
        $user_id = $params['user_id'];

        //personal information
        $nickname = $params['nickname'];
        $age = $params['age'];  //int
        $city = $params['city'];
        $prov = $params['prov'];
        $gender = $params['gender'];
        $hk_uni = $params['hk_uni'];
        $gra_sch = $params['gra_sch'];
        $is_smoke = $params['is_smoke'];
        $rel_status = $params['rel_status'];
        $personality = $params['personality'];
        $habit = $params['habit'];
        $remark = $params['remark'];
        $contact = $params['contact'];
        $head_icon = $params['head_icon'];

        //rental information
        $budget = $params['budget'];  //int
        $num_stu_rt = $params['num_stu_rt'];  //int
        $building_type = $params['building_type'];
        $room_type = $params['room_type'];
        $is_mixed = $params['is_mixed'];
        $avail_hk_time = $params['avail_hk_time'];
        $others = $params['others'];


        //init params
        $conn = new mysqli($this->servername,$this->username,$this->password,$this->dbname);
        $conn->set_charset("utf8");

        //init sql
        $sql = "UPDATE user_info SET nickname='$nickname',age=$age,city='$city',prov='$prov',gender='$gender',hk_uni='$hk_uni',gra_sch='$gra_sch',is_smoke='$is_smoke',rel_status='$rel_status',personality='$personality'
                ,habit='$habit',remark='$remark',contact='$contact',head_icon='$head_icon',budget=$budget,num_stu_rt=$num_stu_rt,building_type='$building_type',room_type='$room_type',is_mixed='$is_mixed',avail_hk_time='$avail_hk_time',others='$others'
                WHERE user_id='$user_id'";

        $conn->query($sql);
        $affRows = $conn->affected_rows;

        if($affRows>0){
            $res['msg'] = "yes";
        }else{
            $res['msg'] = "no";
        }

        //set passing back data
        $res['affRows'] = $affRows;
        $res['sql'] = $sql;

        return $res;

    }

    public function updateUserAuth($userId,$command){

        //inti parpam
        global $sql,$res;
        $res = array();
        $conn = $this->getConn();

        if($command === "pass"){
            $sql = "update register_info set user_auth = '1' where user_id = '$userId'";
        }
        if($command === "reject"){
            $sql = "update register_info set user_auth = '2' where user_id = '$userId'";
        }


        $conn->query($sql);
        $affRows = $conn->affected_rows;

        if($affRows>0){
            $res['msg'] = "success";
        }else{
            $res['msg'] = "failed";
            $res['errorMsg'] = "update userAuth failed. <br>Error: " . $sql . "<br>" . $conn->error;
        }

        $res['sql'] = $sql;

        $conn->close();
        return $res;

    }

    public function getSearchRes($command,$searchSql){

        $conn = $this->getConn();
        $res = array();
        $starNum = 0;

        if($command === "init"){
            $sql = "SELECT us.*,re.user_auth FROM register_info re INNER JOIN user_info us where us.user_id = re.user_id;";
        }elseif($command === "search"){
            $sql = $searchSql;
        }

        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $row['gender'] = $this->dicParse("dict_gender",$row['gender']);
                $row['hk_uni'] = $this->dicParse("dic_hkuni",$row['hk_uni']);
                $row['avail_hk_time'] = $this->dicParse("dic_hktime",$row['avail_hk_time']);
                $row['num_stu_rt'] = $this->dicParse("dic_romunm",$row['num_stu_rt']);
                $row['budget'] = $this->dicParse("dic_budget",$row['budget']);
                $row['is_mixed'] = $this->dicParse("dic_ismixed",$row['is_mixed']);
                $row['room_type'] = $this->dicParseCom("dic_rompre",$row['room_type']);
                $row['building_type'] = $this->dicParseCom("dic_budtype",$row['building_type']);


                $res[$starNum] = $row;
                $starNum++;
            }
            $res['msg'] = "success";
        } else {
            $res['errorMsg'] = "no results" . $conn->error;
            $res['msg'] = "fail";
        }

        $result->close();
        $conn->close();

        return $res;

    }

    public function parsePerson($userInfo){

        $userInfo['gender'] = $this->dicParse("dict_gender",$userInfo['gender']);
        $userInfo['hk_uni'] = $this->dicParse("dic_hkuni",$userInfo['hk_uni']);
        $userInfo['avail_hk_time'] = $this->dicParse("dic_hktime",$userInfo['avail_hk_time']);
        $userInfo['num_stu_rt'] = $this->dicParse("dic_romunm",$userInfo['num_stu_rt']);
        $userInfo['budget'] = $this->dicParse("dic_budget",$userInfo['budget']);
        $userInfo['is_mixed'] = $this->dicParse("dic_ismixed",$userInfo['is_mixed']);
        $userInfo['room_type'] = $this->dicParseCom("dic_rompre",$userInfo['room_type']);
        $userInfo['building_type'] = $this->dicParseCom("dic_budtype",$userInfo['building_type']);

        return $userInfo;

    }

    public function dicParseCom($dic_name,$val){
        $arr = str_split(str_replace(",","",$val));
        $reVal = "";
        $i = 0;
        $numItems = count($arr);
        if($val != "" && $val != null){
            foreach($arr as $v){
                if(++$i === $numItems) {
                    $reVal = $reVal . $this->dicParse($dic_name,$v);
                }else{
                    $reVal = $reVal . $this->dicParse($dic_name,$v) . "/  ";
                }
            }
        }
        return $reVal;
    }

    public function countVote(){
        //get connection
        $conn = $this->getConn();
        //define sql
        $sql = "SELECT count(1) num FROM register_info where vote is not null";

        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            $res = $result->fetch_assoc();
        }else{
            $res['errorMsg'] = "No data found. <br>Error: " . $sql . "<br>" . $conn->error;
        }


        $result->close();
        $conn->close();
        return $res['num'];

    }

    public function countMsg(){
        return $this->count("SELECT count(1) num FROM user_info where bless_word is not null");
    }

    public function updateTmStatus($pUserId,$pTmStatus){
        //inti parpam
        $res = array();
        $conn = $this->getConn();
        $sql = "update register_info set tm_status = '$pTmStatus' where user_id = '$pUserId'";

        $conn->query($sql);
        $affRows = $conn->affected_rows;

        if($affRows>0){
            $res['msg'] = "success";
        }else{
            $res['msg'] = "failed";
            $res['errorMsg'] = "update userAuth failed. <br>Error: " . $sql . "<br>" . $conn->error;
        }

        $res['sql'] = $sql;

        $conn->close();
        return $res;
    }

    public function getTeamInfoMember($pMemberId){
        $leaderId = $this->getTmLeaderId($pMemberId);
        return $this->getTeamInfo($leaderId,"m");
    }

    public function getTeamInfoLeader($pMemberId){
        $leaderId = $this->getTmLeaderId($pMemberId);
        return $this->getTeamInfo($leaderId,"l");
    }

    public function getTeamInfo($pLeaderId,$tag){
        $conn = $this->getConn();
        $sql = "SELECT * FROM tm_reg_info WHERE leader_id = '$pLeaderId'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            $res = $result->fetch_assoc();
            $res['msg'] = "success";
        }else{
            $res['msg'] = "failed";
            $res['errorMsg'] = "No data found. <br>Error: " . $sql . "<br>" . $conn->error;
        }
        $res['teamMember'] = $this->getTeamMemmber($pLeaderId);
        $res['teamMember'] = $this->convertMemberToHtml($pLeaderId,$res['teamMember'],$tag);
        $result->close();
        $conn->close();

        return $res;
    }

    public function getTeamMemmber($pLeaderId){
        $conn = $this->getConn();
        $sql = "SELECT * FROM tm_member where tm_leader_id = '$pLeaderId'";
        $res = array();
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $mInfo = $this->selUserInfo($row['tm_member_id']);
                array_push($res,$mInfo);
            }
        }
        $result->close();
        $conn->close();
        return $res;
    }

    public function convertMemberToHtml($pLeaderId,$pMembers,$tag){
        $htmlText = "";

         foreach($pMembers as $m){
             $mUserId = $m['user_id'];
             $mNickName = $m['nickname'];
             $headIconPath = $m["head_icon"];
             $headIconHtmlText = $m["head_icon"] == "" ? "<div style='height: 110px;width='100px''><img src='img/default-user.jpg' width='100px' style='margin-top: -10px; margin-bottom: 10px'></div>" : "<div style='height: 110px;width=100px;overflow: hidden;'><img src='profile_pic/$headIconPath' width='100px' class='img-rounded'></div>";

             //Member html
             if($tag === 'm'){
                 if($pLeaderId == $mUserId){
                     $mNickName = $mNickName . "(队长)";
                 }
                 $htmlText = $htmlText . "<div class='col-xs-2 team-member-info' style='background-color: #ffffff; border-radius: 6px; margin-left: 20px;'>" .
                     "<div style='margin-bottom:10px;'><a style='color:#BDC3C7;cursor:pointer;margin-left:100px;'><span class='fui-cross-circle'></span></a>".
                     $headIconHtmlText.
                     "<span >$mNickName</span></div>".
                     "</div>";
             }

             //leader html
             if($tag === 'l'){
                 if($pLeaderId != $mUserId){
                     $htmlText = $htmlText . "<div id='$mUserId' class='col-xs-2 team-member-info' style='background-color: #ffffff; border-radius: 6px; margin-left: 20px;'>" .
                         "<div style='margin-bottom:10px;'><a class='removeTm' rel='$mUserId' style='color:#E74C3C;cursor:pointer;margin-left:100px;'><span class='fui-cross-circle'></span></a>".
                         $headIconHtmlText.
                         "<span >$mNickName</span></div>".
                         "</div>";
                 }

             }
         }
        return $htmlText;
    }



    public function getTmLeaderId($pMemberId){
        //get connection
        $conn = $this->getConn();
        //define sql
        $sql = "SELECT tm_leader_id FROM tm_member where tm_member_id = '$pMemberId'";

        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            $res = $result->fetch_assoc();
        }else{
            $res['errorMsg'] =  "No data found. <br>Error: " . $sql . "<br>" . $conn->error;
        }


        $result->close();
        $conn->close();
        return $res['tm_leader_id'];
    }

    public function isSqlSuccess($mAffRows){
        if($mAffRows>0){
            return $msg = "success";
        }else{
            return $msg = "failed";
        }
    }

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * 注意：服务器需要开通fopen配置
     * @param $word 要写入日志里的文本内容 默认值：空值
     */
    public function logResult($word='') {
        $fp = fopen("log.txt","a");
        flock($fp, LOCK_EX) ;
        fwrite($fp,"执行日期：".strftime("%Y%m%d%H%M%S",time())."\n".$word."\n");
        flock($fp, LOCK_UN);
        fclose($fp);
    }

    public function getUserMsg($pUserId){
        $conn = $this->getConn();
        $curDate = date("Ymd");
        $sql = "SELECT * FROM message WHERE ((user_id = '$pUserId' and end_date > '$curDate') or is_system = '1') and msg_status = '1'";
        $msgHtml = "";
        $result = $conn->query($sql);

        if ($result->num_rows > 0){
            while($row = $result->fetch_assoc()) {
                $msgHtml = $this->assembleAlertMsg($msgHtml,$row);
            }
        }else{
            $this->logResult("No data found. <br>Error: " . $sql . "<br>" . $conn->error);
        }

        $result->close();
        $conn->close();


        return $msgHtml;
    }

    public function assembleAlertMsg($pMsgHtml,$pMsgRow){
        //消息类型说明见表dic_info, dict_name_en = 'dic_msg_type'
        //1:messgae  2:success  3:error  4:warning
        $type = $this->dicParse("dic_msg_type",$pMsgRow['type']);
        $msg = $pMsgRow['message'];
        $delay = $pMsgRow['delay'];
        $url = $pMsgRow['url'];
        $endDate = $pMsgRow['end_date'];

        $assAlMsg = "alertify.notify('$msg', '$type', $delay";
        if($url != ""){
            $assAlMsg = $this->addUrlFunction($assAlMsg,$url);
        }
        if($endDate === "99999999"){
            $functionName = "deleteAllMsg();";
            $assAlMsg = $this->addFunction($assAlMsg,$functionName);
        }
        $assAlMsg = $assAlMsg.");";

        return $pMsgHtml.$assAlMsg;
    }

    public function addUrlFunction($pMsgHtml,$pUrl){
        return $pMsgHtml . ", function(){window.location.assign('$pUrl');}";
    }
    public function addFunction($pMsgHtml,$pFunctionName){
        return $pMsgHtml . ", function(){ $pFunctionName }";
    }

    public function generalUpdate($pSql){
        //inti parpam
        $res = array();

        $conn = $this->getConn();
        $conn->query($pSql);

        $affRows = $conn->affected_rows;
        if($affRows>0){
            $res['msg'] = "success";
        }else{
            $res['msg'] = "failed";
            $this->logResult("Error: " . $pSql . "<br>" . $conn->error);
        }
        $res['sql'] = $pSql;
        $conn->close();

        return $res;
    }

    public function generalInsert($pSql){
        $conn = $this->getConn();
        $res = array();

        if ($conn->query($pSql) === TRUE) {
            $res['msg'] = "success";
        } else {
            $res['msg'] = "failed";
            $this->logResult("Error: " . $pSql . "<br>" . $conn->error);
        }

        $res['sql'] = $pSql;

        $conn->close();
        return $res;
    }

    public function generalDelete($pSql){
        $conn = $this->getConn();
        $res = array();

        if ($conn->query($pSql) === TRUE) {
            $res['msg'] = "success";
        } else {
            $res['msg'] = "failed";
            $this->logResult("Error: " . $pSql . "<br>" . $conn->error);
        }

        $res['sql'] = $pSql;

        $conn->close();
        return $res;
    }

    public function generalSelectSingleRow($pSql){
        $res = array();
        $conn = $this->getConn();

        $result = $conn->query($pSql);

        if ($result->num_rows > 0){
            $res = $result->fetch_assoc();
            $res['msg'] = "success";
        }else{
            $res['msg'] = "failed";
            $this->logResult("Error: " . $pSql . "<br>" . $conn->error);
        }

        $res['sql'] = $pSql;

        $result->close();
        $conn->close();

        return $res;
    }
}