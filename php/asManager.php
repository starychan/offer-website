<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 4/17/15
 * Time: 4:39 下午
 */
include "asDBManager.php";

$command = $_GET['command'];
$dataBack = array();
$params   = array();
$dbm = new dbManager;
$asdb = new asDBManager;

// validate the variables
// if any of these variables don't exist, add an error to our $errors array

switch ($command) {
    case "saveAsRegInfo":
        $name = $_GET['name'];
        $phone = $_GET['phone'];
        $place = $_GET['place'];
        $dataBack = $asdb->saveAsRegInfo($name,$phone,$place);
        echo json_encode($dataBack);
        break;
}