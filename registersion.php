<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>

    <meta charset="utf-8" />
    <title>报名-港校副学士精英计划2015</title>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="less/animate.less-master/animate.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="js/woothemes-FlexSlider-06b12f8/flexslider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="js/prettyPhoto_3.1.5/prettyPhoto.css" type="text/css" media="screen" />
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link rel='stylesheet' id='BNS-Corner-Logo-Style-css'  href='social_icons.css' type='text/css' media='screen' />
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css" media="screen" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="fonts/font-awsome/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="js/modernizr.custom.48287.js"></script>
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-57x57-precomposed.png" />
    <link rel="shortcut icon" href="favicon.png" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

    <!-- include alert-->
    <script src="dist/js/vendor/alertify.min.js"></script>
    <link rel="stylesheet" href="dist/css/vendor/alertify.min.css" />
    <link rel="stylesheet" href="dist/css/vendor/default.min.css" />
<body>
<header>
    <div class="container">
        <div class="navbar">
            <div class="navbar-inner"> <a class="brand" href="associate.html"> <img src="images/offer_logo_header.png" width="260" height="90" alt="OFFERHK" /></a>
<!--                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="nb_left pull-left"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></span> <span class="nb_right pull-right">menu</span> </a>-->
<!--                <div class="nav-collapse collapse">-->
<!--                    <ul class="nav pull-right">-->
<!--                        <li class="active"><a href="index.html">主页</a></li>-->
<!--                        <li><a href="application.html">留学申请</a></li>-->
<!--                        <li><a href="services.html">在港服务</a></li>-->
<!--                        <li><a href="coursetrip.html">游学项目</a></li>-->
<!--                        <li><a href="news.html">最新资讯</a></li>-->
<!--                        <li><a href="contact.html">联系我们</a></li>-->
<!--                    </ul>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</header>
<div id="main">
    <div class="container">
        <section id="contact">
            <div class="hgroup">
                <h1>报名奥弗宣讲会</h1>
                <h3 style="color: #0082cb;"><a href="associate.html">港校副学士精英计划2015</a></h3>
            </div>
            <div class="row">
                <div class="span5 contact_form">
                    <div class="row">
                        <div class="span5">
                            <h3>姓名</h3>
                            <input id="name" type="text" class="span5" placeholder="" style="min-height:50px; font-size: 25px;"/>
                        </div>
                        <div class="span5" style="margin-top: 30px;">
                            <h3>联系电话</h3>
                            <input id="phone" type="text" class="span5" style="min-height:50px; font-size: 25px;"/>
                        </div>
                        <div class="span5" style="margin-top: 30px;">
                            <h3>参加宣讲会地点</h3>
                            <select id="place" class="span5" style="min-height:40px; height: 50px; font-size: 20px;">
                                <option value="">点击选择参加宣讲会地点</option>
                                <option value="1">6月10日 晚上19时30分 太原市第一场</option>
                                <option value="2">6月13日 下午15时 太原市第二场</option>
                                <option value="3">6月14日 下午15时 广州市</option>
                                <option value="4">6月14日 下午15时 武汉汉口</option>
                                <option value="5">6月17、19、20日 晚上 QQ网上宣讲会</option>
                            </select>
                        </div>
                        <div class="span5" style="margin-top: 20px; text-align: center;margin-bottom: 20px; "> <button id="subm" class="btn btn-large btn-success"><span style="font-size: 20px;font-weight: bold;">现在报名</span></button> </div>
                        <div style="margin-top:50px; text-align: left;"><p>如有疑问请与我们联系：</p></div>
                        <div class="span5" style="text-align: center;">
                            <span style="font-size: 17px;">太原站：18588435524 (崔老师)<br> 广州站：15013133398 (张老师)<br>武汉站：14714319385 (梅老师)</span>
                        </div>

                        <div class="span5" style="margin-top: 50px; text-align: center;margin-bottom: 20px;">
                            <a style="margin-bottom:20px;font-size: 20px;">️详细宣讲会<br>⬇️⬇地点及时间请见宣传单页⬇️⬇️</a>
                            <a href="img/prom.jpg" ><img src="img/prom.jpg" style="margin-top:15px;margin-bottom:5px;"></a>
                            <a href="img/prom2.jpg"><img src="img/prom2.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript" src="js/jquery-latest.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>')</script>
<script type="text/javascript" src="js/modernizr.custom.48287.js"></script>
<script src="js/woothemes-FlexSlider-06b12f8/jquery.flexslider-min.js"></script>
<script src="js/prettyPhoto_3.1.5/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="js/isotope/jquery.isotope.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.ui.totop.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript" src="js/restart_theme.js"></script>

<script type="text/javascript">
    $("#subm").click(function(){
        //pre-work, switch div and remove child of div
        var name = $("#name").val();
        var phone = $("#phone").val();
        var place = $("#place").val();

        if(name == null || name == ""){
            alertify.alert("请填写姓名");
            return;
        }
        if(phone == null || phone == ""){
            alertify.alert("请填写联系电话");
            return;
        }
        if(place == null || place == ""){
            alertify.alert("请选择将要参加的宣讲会时间及地点");
            return;
        }
        //start ajax
        var settings = {
            type: "GET",
            url: "php/asManager.php",
            encode:true,
            data: {"command":"saveAsRegInfo","name":name,"phone":phone,"place":place}
        };

        $.ajax(settings).done(function(data){
            
            var res = jQuery.parseJSON(data);
            if(res.msg == 'success'){
                alertify.alert('您已成功报名港校副学士精英计划2015宣讲会。由于名额有限，请务必准时到达宣讲会地点(详细宣讲会地点及时间请见宣传单页)。<br>谢谢您的配合').set('onok', function(){ window.location = "associate.html";} );
            }
        });
    });
</script>

</body>
</html>